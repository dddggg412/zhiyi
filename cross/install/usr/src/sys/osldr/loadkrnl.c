/****************************************************************************************************
 * 描述：内核加载
 * www.880.xin
 ****************************************************************************************************/


#include <os.h>
#include <os/pdir.h>
#include <os/tss.h>
#include <os/seg.h>
#include <os/syspage.h>
#include <os/mbr.h>
#include <os/dfs.h>
#include <os/pe.h>
#include <os/dev.h>

void kprintf(const char *fmt,...);
void panic(char *msg);

extern unsigned long kernel_entry;
extern int bootpart;

int boot_read(void *buffer, size_t count, blkno_t blkno);
char *alloc_heap(int numpages);

char bsect[SECTORSIZE];
char ssect[SECTORSIZE];
char gsect[SECTORSIZE];
char isect[SECTORSIZE];
blkno_t blockdir[1024];

/*加载操作系统内核*/
void load_kernel(int bootdrv)
{
	struct master_boot_record *mbr;
	struct superblock *sb;
	struct groupdesc *group;
	struct inodedesc *inode;
	int blocksize;
	int blks_per_sect;
	int kernelsize;
	int kernelpages;
	char *kerneladdr;
	struct dos_header *doshdr;
	struct image_header *imghdr;
	char *addr;
	blkno_t blkno;
	int i;
	int j;
	pte_t *pt;
	int imgpages;
	int start;
	char *label;
	struct boot_sector *bootsect;
	// 如果从硬盘启动，则确定活动启动分区
	if (bootdrv & 0x80 && (bootdrv & 0xF0) != 0xF0)
	{
		mbr = (struct master_boot_record *) bsect;
		if (boot_read(mbr, SECTORSIZE, 0) != SECTORSIZE)
		{
			panic("Unable to read main boot record");
		}

		if (mbr->signature != MAIN_BOOT_RECORD_SIGNATURE )
		{
			panic("Invalid startup signature");
		}

		bootsect = (struct boot_sector *) bsect;
		label = bootsect->label;
		if (label[0] == 'Z' && label[1] == 'H' && label[2] == 'I' && label[3] == 'Y' && label[4] == 'I')
		{
		    // 磁盘没有分区表
		    start = 0;
		    bootpart = -1;
		} else
		{
			// 查找活动分区
			bootpart = -1;
			for (i = 0; i < 4; i++)
			{
				if (mbr->parttab[i].bootid == 0x80)
				{
					bootpart = i;
					start = mbr->parttab[i].relsect;
				}
			}
			if (bootpart == -1)
			{
			    panic("There are no bootable partitions on the boot drive");
			}
		}
	} else
	{
	    start = 0;
	    bootpart = 0;
	}
	// 从引导设备读取超级块
	sb = (struct superblock *) ssect;
	if (boot_read(sb, SECTORSIZE, 1 + start) != SECTORSIZE)
	{
		panic("Unable to read super block from boot device");
	}
	// 检查签名和版本
	if (sb->signature != DFS_SIGNATURE)
	{
		panic("invalid DFS signature");
	}
	if (sb->version != DFS_VERSION)
	{
		panic("invalid DFS version");
	}
	blocksize = 1 << sb->log_block_size;
	blks_per_sect =  blocksize / SECTORSIZE;
	// 读取第一个组描述符
	group = (struct groupdesc *) gsect;
	if (boot_read(group, SECTORSIZE, sb->groupdesc_table_block * blks_per_sect + start) != SECTORSIZE)
	{
	    panic("unable to read group descriptor from boot device");
	}

	// 读取内核的inode
	inode = (struct inodedesc *) isect;
	if (boot_read(isect, SECTORSIZE, group->inode_table_block * blks_per_sect + start) != SECTORSIZE)
	{
	    panic("unable to read kernel inode from boot device");
	}
	inode += DFS_INODE_KRNL;

	// 计算内核大小
	kernelsize = (int) inode->size;
	kernelpages = PAGES(kernelsize);
	kprintf("Kernel size %d KB\n", kernelsize / 1024);

	// 为内核分配页表
	if (kernelpages > PTES_PER_PAGE)
	{
		panic("kernel too big");
	}
	pt = (pte_t *) alloc_heap(1);
	pdir[PDEIDX(OSBASE)] = (unsigned long) pt | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;

	// 为内核分配页面
	kerneladdr = alloc_heap(kernelpages);

	// 从引导设备读取内核
	if (inode->depth == 0)
	{
		addr = kerneladdr;
		for (i = 0; i < (int) inode->blocks; i++)
		{
			if (boot_read(addr, blocksize, inode->blockdir[i] * blks_per_sect + start) != blocksize)
			{
				panic("error reading kernel from boot device");
			}
			addr += blocksize;
		}
	} else if (inode->depth == 1)
	{
		addr = kerneladdr;
		blkno = 0;
		for (i = 0; i < DFS_TOPBLOCKDIR_SIZE; i++)
		{
			if (boot_read(blockdir, blocksize, inode->blockdir[i] * blks_per_sect + start) != blocksize)
			{
			    panic("error reading kernel inode dir from boot device");
			}
			for (j = 0; j < (int) (blocksize / sizeof(blkno_t)); j++)
			{
				if (boot_read(addr, blocksize, blockdir[j] * blks_per_sect + start) != blocksize)
				{
				    panic("error reading kernel inode dir from boot device");
				}
				addr += blocksize;
				blkno++;
				if (blkno == inode->blocks)
				{
					break;
				}
			}

			if (blkno == inode->blocks) break;
		}
	} else
	{
		panic("unsupported inode depth");
	}

	// 确定内核的入口点
	doshdr = (struct dos_header *) kerneladdr;
	imghdr = (struct image_header *) (kerneladdr + doshdr->e_lfanew);
	kernel_entry = imghdr->optional.address_of_entry_point + OSBASE;

	// 为.data节分配页面
	imgpages = PAGES(imghdr->optional.size_of_image);
	alloc_heap(imgpages - kernelpages);

	// 重新定位资源数据并清除未初始化的数据
	if (imghdr->header.number_of_sections == 4)
	{
		struct image_section_header *data = &imghdr->sections[2];
		struct image_section_header *rsrc = &imghdr->sections[3];
		memcpy(kerneladdr + rsrc->virtual_address, kerneladdr + rsrc->pointer_to_raw_data, rsrc->size_of_raw_data);
		memset(kerneladdr + data->virtual_address + data->size_of_raw_data, 0, data->virtual_size - data->size_of_raw_data);
	}

	// 将内核映射到虚拟地址空间
	for (i = 0; i < imgpages; i++)
	{
		pt[i] = (unsigned long) (kerneladdr + i * PAGESIZE) | PAGE_TABLE_PRESENT | PAGE_TABLE_WRITE_ABLE;
	}
}
