;
; 操作系统启动过程步骤3：实模式初始化
; www.880.xin
;

; 加载os加载程序后，引导程序（bootsector）调用ldrinit。ldrinit使用简化的DOS.EXE格式。它作为dll的DOS存根附加到osldr.dll
; os加载器在固定地址0x90000处加载。
; 这允许加载程序、数据区和堆栈使用56K。

OSLDRSEG    equ 0x9000
OSLDRBASE   equ (OSLDRSEG * 16)   ;OSLDRBASE = 0X90000

; 将堆栈8K置于640K边界以下，以便为扩展BIOS数据区留出空间

STACKTOP    equ 0x9e000

        BITS    16
        SECTION .text

;
; EXE文件头
;

textstart:

header_start:
        db      0x4d, 0x5a          ; EXE文件签名
        dw      textsize % 512
        dw      (textsize + 511) / 512
        dw      0                   ; 重定位信息：无
        dw      (header_end - header_start) / 16 ; 段落中的页眉大小
        dw      0                   ; 最小额外内存
        dw      0xffff              ; 最大额外内存
        dw      0                   ; 初始SS（修复前）
        dw      0                   ; 初始SP
        dw      0                   ; （否）校验和
        dw      start               ; 初始IP
        dw      0                   ; 初始CS（修复前
        dw      header_end          ; 重新定位表的文件偏移量：无
        dw      kernel_options            ; 覆盖编号（内核选项的偏移量）
        align   64, db 0
header_end:

;------------------------------------------------------2.引导程序的入口点--------------------------------------------------------

start:
        ; 设置初始环境
        mov     ax, cs
        mov     ds, ax
        mov     es, ax

        ; 保存启动驱动器和启动映像
        mov     [bootdrv], dl
        mov     [bootimg], ebx

        ; 显示启动消息
        mov     si, osldrmsg
        call    print

        ; 尝试从BIOS获取系统内存映射
        call    getmemmap

        ; 检查APM BIOS
        call    apmbioscheck

        ; 将系统移至32位保护模式
        cli                         ; 禁用bios中断。内核初始化后重新启用系统自身提供的中断服务.

        ; 开启A20 地址线
        call    empty8042           ; 等待输入缓冲器空。
        mov     al, 0xd1            ; 命令写入.D1 命令码-表示要写数据到8042 的P2 端口。P2 端
        out     0x64, al            ; P2 端口的位1 用于A20 线的选通。数据要写到60 口。
        call    empty8042           ; 等待输入缓冲器空，看命令是否被接受。
        mov     al,0xdf             ; A20 on 选通A20 地址线的参数。
        out     0x60, al            ; 将al 的值写入0x60,
        call    empty8042           ; 输入缓冲器为空，则表示A20 线已经选通。

        ; 加载引导描述符（GDT、IDT）
        lidt    [idtsel]            ; 用0,0加载idt
        lgdt    [gdtsel]            ; 使用任何适当的方法加载gdt

        ; 切换到保护模式
        mov     ax, 0x0001          ; CR0寄存器:0号32位控制器，存放系统控制标志，第0位为PE（protected Mode Enable）标志,置1时CPU工作在保护模式下，置0时为实模式。
        lmsw    ax                  ;将源操作数加载到机器状态字

        ; 使用引导描述符初始化段寄存器
        mov     ax, flat_data - gdt
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
        mov     ss, ax

        ; 设置代码段并清除预取
        jmp     dword (flat_code - gdt):start32 + OSLDRBASE      ;OSLDRBASE = 0X90000000

start32:    ;将处理器切换到保护模式并调用32位入口点
        BITS    32
        ; 设置堆栈
        mov     esp, STACKTOP  ;STACKTOP    equ 0x9e000
        ; 为保护模式堆栈指针添加空间
        push    esp
        ; 清除标志
        push    dword 2
        popfd    ;在堆栈上压入32位的EFLAGS寄存器的值
        ; 计算入口点
        mov     eax, [OSLDRBASE + 0x3c]
        mov     eax, [eax + OSLDRBASE + 0x28]
        add     eax, OSLDRBASE
        ; 推送操作系统加载程序加载地址和引导参数块
        push    dword 0
        push    dword OSLDRBASE + bootparams ;OSLDRBASE = 0X90000
        push    dword OSLDRBASE

        ; 在os加载器中调用启动代码
        call    eax
        ; 我们再也不回来了
        cli
        hlt

        BITS    16
;-----------------------------------------------------2.打印-子程序---------------------------------------------------------
print:
        push    ax
        cld
nextchar:
        mov     al, [si]
        cmp     al, 0
        je      printdone
        call    printchar
        inc     si
        jmp     nextchar
printdone:
        pop     ax
        ret
printchar:
        mov     ah, 0x0e
        int     0x10
        ret
;-----------------------------------------------------2.清空键盘命令队列-子程序---------------------------------------------------------
empty8042:
        dw      0x00eb, 0x00eb  ; 输入/输出后需要延迟。jmp $+2 ,jmp $+2  ,$ 表示当前指令的地址。这是两个跳转指令的机器码(跳转到下一句)，相当于延时空操作。
        in      al,0x64         ; 8042状态端口
        test    al,2            ; 输入缓冲区已满吗？
        jnz     empty8042       ; 标志zf=0，即d0=1，则程序转移到empty8042，实现循环。
        ret
;-----------------------------------------------------2.APM BIOS检查-子程序---------------------------------------------------------

apmbioscheck:
        mov     ax, 0x5300          ; APM BIOS安装检查
        xor     bx, bx
        int     0x15
        jc      apmdone

        cmp     bx, 0x504d          ; 检查“PM”签名
        jne     apmdone

        and     cx, 0x02            ; 是 32-bit 的支持?
        jz      apmdone
        
        mov     [apmversion], ax    ; 记录APM BIOS版本
        mov     [apmflags], cx      ; 和标志

        mov     ax, 0x5304          ; 先断开以防万一
        xor     bx, bx
        int     0x15                ; 忽略返回代码

        mov     ax, 0x5303          ; APM BIOS 32位连接
        xor     ebx, ebx
        xor     cx, cx
        xor     dx, dx
        xor     esi, esi
        xor     di, di
        int     0x15
        jc      no32apmbios
        
        mov     [apmcseg32], ax
        mov     [apmentry], ebx
        mov     [apmcseg16], cx
        mov     [apmdseg], dx
        mov     [apmcseg32len], si
        shr     esi, 16
        mov     [apmcseg16len], si
        mov     [apmdseglen], di
        
        mov     ax, 0x5300          ; 重做APM BIOS安装检查
        xor     bx, bx
        xor     cx, cx
        int     0x15
        jc      apmdisconnect

        jmp     apmdone

apmdisconnect:
        mov     ax, 0x5304          ; 断开
        xor     bx, bx
        int     0x15
        jmp     apmdone
        
no32apmbios:
        mov     cx, 0xfffd          ; 移除32位支持位
        and     [apmflags], cx

apmdone:
        ret
;-----------------------------------------------------2.从BIOS获取内存映射-子程序---------------------------------------------------------

SMAP       equ 0x534d4150
MAXMEMRECS equ 32
MEMRECSIZ  equ 20
   
getmemmap:
        ; 使用int 15 ax=0xe80获取内存映射
        xor     ebx, ebx            ; 连续值
        mov     di, memrecs         ; 指向内存映射的指针

e820loop:
        ; 获取下一个内存映射入口
        mov     eax, 0x0000e820     ; eax = E820
        mov     edx, SMAP           ; edx = ASCII 'SMAP'
        mov     ecx, MEMRECSIZ      ; ecx=内存条目的大小
        push    ds                  ; es:di=内存映射的地址
        pop     es
        int     0x15
        jc      e820fail

        cmp     eax, SMAP           ; 检查返回是否为“SMAP”
        jne     e820fail

        ; 保存新的内存条目
        mov     eax, [nrmemrecs]    ; 检查内存记录的最大数量
        cmp     eax, MAXMEMRECS
        jnl     e820fail

        inc     eax                 ; 向前移动到内存映射中的下一条记录
        mov     [nrmemrecs], eax
        add     di, MEMRECSIZ
        
        ; 检查更多内存记录
        cmp     ebx, 0
        jne     e820loop

        ret

e820fail:
        xor     eax, eax            ; 重置内存映射
        mov     [nrmemrecs], eax
        ret
;------------------------------------------------------2.全局描述符表--------------------------------------------------------

DESCRIPTOR_DATA          equ     0x1000
DESCRIPTOR_CODE          equ     0x1800

DESCRIPTOR_WRITE         equ     0x200
DESCRIPTOR_READ          equ     0x200
DESCRIPTOR_CONFORM       equ     0x400

DESCRIPTOR_BIG           equ     0x40
DESCRIPTOR_BIG_LIM       equ     0x80

DESCRIPTOR_PRESENT       equ     0x8000

%macro segdesc 3
        dw      (%2 & 0xffff)
        dw      (%1 & 0xffff)
        db      (%1) >> 16
        db      ((%3) | DESCRIPTOR_PRESENT) >> 8
        db      ((%3) & 0xff) | ((%2) >> 16)
        db      (%1) >> 24
%endmacro

idtsel:
        dw      0               ; idt limit = 0
        dw      0,0             ; idt base = 0L

gdtsel:
        dw      gdtlen
        dd      gdt + OSLDRBASE

        align   8
gdt:                                   ;GDT是保护模式下管理段描述符的数据结构。在系统中唯一的存放段寄存器内容的数组，配合程序进行保护模式下的段寻址。存放每一个任务的局部描述符（LDT）地址和任务状态段（TSS）地址,他完成进程中各段的寻址，现场保护与现场恢复。
null_desc       segdesc 0,0,0                                              ;第一项：空
flat_code       segdesc 0, 0x0fffff, DESCRIPTOR_CODE | DESCRIPTOR_READ | DESCRIPTOR_BIG | DESCRIPTOR_BIG_LIM   ;第二项：内核代码段描述符
flat_data       segdesc 0, 0x0fffff, DESCRIPTOR_DATA | DESCRIPTOR_WRITE | DESCRIPTOR_BIG | DESCRIPTOR_BIG_LIM  ;第三项：内核数据段描述符
real_code       segdesc OSLDRBASE, 0x0ffff, DESCRIPTOR_CODE | DESCRIPTOR_READ | DESCRIPTOR_CONFORM    ;第四项：实模式代码段描述符
real_data       segdesc OSLDRBASE, 0x0ffff, DESCRIPTOR_DATA | DESCRIPTOR_WRITE               ;第四项：实模式数据段描述符
gdtlen          equ $ - gdt - 1

;
; 引导参数块
;

bootparams:

bootdrv      dd    0        ; 引导驱动器
bootimg      dd    0        ; RAM磁盘映像地址

kernel_options     times 128 db 0 ; 内核选项

apmversion   dw    0        ; APM版本（BCD格式）
apmflags     dw    0        ; 来自安装检查的APM标志
apmcseg32    dw    0        ; APM 32位代码段（实模式段基地址）
apmentry     dw    0        ; APM BIOS入口点的偏移量
apmcseg16    dw    0        ; APM 16位代码段（实模式段基地址）
apmdseg      dw    0        ; APM数据段（实模式段基地址）
apmcseg32len dw    0        ; APM BIOS 32位代码段长度
apmcseg16len dw    0        ; APM BIOS 16位代码段长度
apmdseglen   dw    0        ; APM BIOS数据段长度

nrmemrecs    dd    0        ; 系统存储器映射
memrecs      times (MAXMEMRECS * MEMRECSIZ) db 0

osldrmsg:    db    ', 2:ldrinit.asm', 0

textend:
textsize equ (textend - textstart)
