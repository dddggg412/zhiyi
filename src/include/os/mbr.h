/****************************************************************************************************
 * 描述：主引导记录
 * www.880.xin
 ****************************************************************************************************/


#ifndef MBR_H
#define MBR_H

#define MAIN_BOOT_RECORD_SIGNATURE            0xAA55

#define ZHIYI_BOOT_PARTITION_ID  0xCC

#pragma pack(push, 1)

/*磁盘分区*/
struct disk_partition {
  unsigned char bootid;           // 可启动的?  0=no, 128=yes
  unsigned char BeginningHeadNumber;  // 起始头编号
  unsigned char BeginningSectorNumber;  // 起始扇区编号
  unsigned char begcyl;   // 10位数字，在BegingSectorNumber中放入高2位
  unsigned char systid;   // Operating System type indicator code
  unsigned char endhead;  // Ending head number
  unsigned char endsect;  // Ending sector number
  unsigned char endcyl;   // Also a 10 bit nmbr, with same high 2 bit trick
  unsigned int relsect;   // First sector relative to start of disk
  unsigned int numsect;   // Number of sectors in partition
};

/*主引导记录*/
struct master_boot_record  {
  char bootstrap[446];
  struct disk_partition parttab[4];
  unsigned short signature;
};

/*引导扇区*/
struct boot_sector {
  char prolog[4];
  char label[8];
  unsigned short ldrsize;
  unsigned long ldrstart;
  char bootstrap[492];
  unsigned short signature;
};

#pragma pack(pop)

#endif
