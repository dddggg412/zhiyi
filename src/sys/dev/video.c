
// 6845屏幕控制器


#include <os/krnl.h>

#define VIDEO_PORT_REG       0x3D4
#define VIDEO_PORT_DATA      0x3D5

#define VIDEO_REG_CURSOR_ZUIGAOWEI 0x0E //MSB(Most Significant Bit)最高有效位代表一个n位二进制数字中的n-1位，具有最高的权值2^(n-1).对于有符号的二进制数，负数采用反码或补码形式，此时MSB用来表示符号，msb为1表示负数，0表示正数。
#define VIDEO_REG_CURSOR_ZUIDIWEI 0x0F //LSB(Least Significant Bit)--最低有效位.LSB代表二进制中最小的单位，可以用来指示数字很小的变化。也就是说，LSB是一个二进制数字中的第0位（即最低位），具有权值为2^0，可以用来检测数的奇偶性。

#define LIE                 80                 //列
#define HANG                25                 //行
#define CELLSIZE             2                 //单元格大小
#define LINESIZE             (LIE * CELLSIZE)  //行大小
#define SCREENSIZE           (LINESIZE * HANG) //屏幕大小
#define TABSTOP              8

//0x07 前背景(0黑)后字(7灰)
#define ATTR_HEIJINGBAIZI        0x07  //黑背景白字符(0X07,0黑色，7白色)
#define ATTR_BAIJINGHEIZI        0x70  //白背景黑字符
#define ATTR_FFJINGHEIZI         0x6f  //浅咖啡背景白字符
#define ATTR_ZIJINGHEIZI         0x5f  //紫背景白字符
#define ATTR_HONGJINGHEIZI       0x4f  //红背景白字符
#define ATTR_LVJINGHEIZI         0x30  //绿背景黑字符
#define ATTR_HUANGLVJINGBAIZI    0x2f  //黄绿背景白字符
#define ATTR_LANJINGHEIZI        0x10  //蓝背景黑字符


unsigned char *videoMem;
int cursor_pos; //光标位置
unsigned char video_color = ATTR_HUANGLVJINGBAIZI;
int video_state = 0;
int saved_cursor_pos = 0;
int linewrap = 1;  //换行

static int color_map[8] = {0, 4, 2, 6, 1, 5, 3, 7};

//初始化屏幕
void init_video()
{
  // 设置视频基本地址（使用映射的视频地址）.videoMem=1874829312=0x90406000
  videoMem = (unsigned char *) VIDEO_BASE_ADDRESS;  //VIDEO_BASE_ADDRESS1.系统分页时映射内存地址，2.这里指定显示内存
  // 获取光标位置
  outp(VIDEO_PORT_REG, VIDEO_REG_CURSOR_ZUIGAOWEI);// 光标最高有效位。outb(0x3D4,0x0E)
  cursor_pos = inp(VIDEO_PORT_DATA) & 0xFF;  // cursor_pos = 6 = 0x6
  cursor_pos <<= 8; //cursor_pos = 1536 = 0x600

  outp(VIDEO_PORT_REG, VIDEO_REG_CURSOR_ZUIDIWEI);// 光标最低有效位。
  cursor_pos += inp(VIDEO_PORT_DATA) & 0xFF;  //cursor_pos = 1680 = 0x690
  cursor_pos <<= 1;  //cursor_pos = 3360 = 0xd20
}

void show_cursor() {
  // 设置新光标位置
  unsigned int pos = cursor_pos >> 1;
  outp(VIDEO_PORT_REG, VIDEO_REG_CURSOR_ZUIDIWEI);
  outp(VIDEO_PORT_DATA, pos & 0xFF);
  outp(VIDEO_PORT_REG, VIDEO_REG_CURSOR_ZUIGAOWEI);
  outp(VIDEO_PORT_DATA, pos >> 8);
}

void hide_cursor() {
  // 设置屏幕外的光标位置
  unsigned int pos = LIE * HANG + 1;
  outp(VIDEO_PORT_REG, VIDEO_REG_CURSOR_ZUIDIWEI);
  outp(VIDEO_PORT_DATA, pos & 0x0ff);
  outp(VIDEO_PORT_REG, VIDEO_REG_CURSOR_ZUIGAOWEI);
  outp(VIDEO_PORT_DATA, pos >> 8);
}

void set_cursor(int col, int line) {
  cursor_pos = (line * LIE + col) * CELLSIZE;
}

//清除行
static void clear_line(unsigned char *p) {
  int i;
  unsigned char attr = video_color;

  for (i = 0; i < LIE; i++) {
    *p++ = ' ';
    *p++ = attr;
  }
}

// 向上滚动屏幕
static void scroll_up()
{
  memcpy(videoMem, videoMem + LINESIZE, SCREENSIZE - LINESIZE);

  // 清除底部行
  clear_line(videoMem + SCREENSIZE - LINESIZE);
}

// 向下滚动屏幕
static void scroll_down() {
  memcpy(videoMem + LINESIZE, videoMem, SCREENSIZE - LINESIZE);

  // 清除顶行
  clear_line(videoMem);
}
//句柄序列
static void handle_sequence(int x, int y, char ch) {
  switch (ch) {
    case 'H': { // Position
      int line = x;
      int col = y;

      if (line < 1) line = 1;
      if (line > HANG) line = HANG;
      if (col < 1) col = 1;
      if (col > LIE) col = LIE;
      set_cursor(col - 1, line - 1);
      break;
    }
    
    case 'J': // 清屏/eos
      if (x == 1)
      {
        unsigned char *p = videoMem + cursor_pos;
        while (p < videoMem + SCREENSIZE)
        {
          *p++ = ' ';
          *p++ = video_color;
        }
      } else {
        unsigned char *p = videoMem;
        while (p < videoMem + SCREENSIZE) {
          *p++ = ' ';
          *p++ = video_color;
        }
        cursor_pos = 0;
      }
      break;
    
    case 'K': { // Clear to end of line
      int pos = cursor_pos;
      do {
        videoMem[pos++] = ' ';
        videoMem[pos++] = video_color;
      } while ((pos) % LINESIZE != 0);
      break;
    }
    
    case 'm': // 设置字符增强
      // 修改为ANSI颜色属性3/15/07-C Girdosky
      if (x >= 30 && x <= 37)
      {
        // 前景颜色
        video_color = color_map[x - 30] + (video_color & 0xF8);
      } else if (x >= 40 && x <= 47)
      {
        // 背景颜色
        video_color = (color_map[x - 40] << 4) + (video_color & 0x8F);
      } else if (x == 1)
      {
        // 高强度前景
        video_color = video_color | 8;
      } else if (x == 2)
      {
        // 低强度前景
        video_color = video_color & ~8;
      } else if (x == 5) {
        // 高强度背景
        video_color = video_color | 128;
      } else if (x == 6) {
        // 低强度背景
        video_color = video_color & ~128;
      } else if (x == 7) {
        // Reverse
        video_color = ((video_color & 0xF0) >> 4) + ((video_color & 0x0F) << 4); 
      } else if (x == 8) {
        // Invisible make forground match background
        video_color = ((video_color & 0xF0) >> 4) + (video_color & 0xF0);
      } else {
        video_color = ATTR_HEIJINGBAIZI;
      }
      break;

    case 'A':  // Cursor up
      while (x-- > 0) {
        cursor_pos -= LINESIZE;
        if (cursor_pos < 0) {
          cursor_pos += LINESIZE;
          break;
        }
      }
      break;

    case 'B': // Cursor down
      while (x-- > 0) {
        cursor_pos += LINESIZE;
        if (cursor_pos >= SCREENSIZE) {
          cursor_pos -= LINESIZE;
          break;
        }
      }
      break;
    
    case 'C': // Cursor right
      cursor_pos += x * CELLSIZE;
      if (cursor_pos >= SCREENSIZE) cursor_pos = SCREENSIZE - 1;
      break;
    
    case 'D': // Cursor left
      cursor_pos -= x * CELLSIZE;
      if (cursor_pos < 0) cursor_pos = 0;
      break;

    case 'L': // 插入行
      while (x-- > 0) {
        int sol = cursor_pos - cursor_pos % LINESIZE;
        memcpy(videoMem + sol + LINESIZE, videoMem + sol, SCREENSIZE - LINESIZE - sol);
        clear_line(videoMem + sol * LINESIZE);
      }
      break;

    case 'M': // 删除行
      while (x-- > 0) {
        int sol = cursor_pos - cursor_pos % LINESIZE;
        memcpy(videoMem + sol, videoMem + sol + LINESIZE, SCREENSIZE - LINESIZE - sol);
        clear_line(videoMem + SCREENSIZE - LINESIZE);
      }
      break;

    case '@': // 插入字符
      while (x-- > 0) {
        memcpy(videoMem + cursor_pos + CELLSIZE, videoMem + cursor_pos, SCREENSIZE - cursor_pos - 1);
        videoMem[cursor_pos] = ' ';
        videoMem[cursor_pos + 1] = video_color;
      }
      break;

    case 'P': // 删除字符
      while (x-- > 0) {
        memcpy(videoMem + cursor_pos, videoMem + cursor_pos + CELLSIZE, SCREENSIZE - cursor_pos - 1);
        videoMem[SCREENSIZE - 2] = ' ';
        videoMem[SCREENSIZE - 1] = video_color;
      }
      break;
    
    case 's': // 保存光标
      saved_cursor_pos = cursor_pos;
      break;

    case 'u': // 恢复光标
      cursor_pos = saved_cursor_pos;
      break;

    case 'h': // 启用换行
      if (x == 7) linewrap = 1;
      break;

    case 'l': // 禁止换行
      if (x == 7) linewrap = 0;
      break;
  }
}

//多字符句柄
static int handle_multichar(int state, char ch) {
  static int x, y;

  switch (state) {
    case 1: // Escape has arrived
      switch (ch) {
        case '[': // Extended sequence
          return 2;

        case 'P': // 光标向下一行
          cursor_pos += LINESIZE;
          if (cursor_pos >= SCREENSIZE) cursor_pos -= LINESIZE;
          return 0;

        case 'K': // 光标向左一定一个
          if (cursor_pos > 0) cursor_pos -= CELLSIZE;
          return 0;

        case 'H': // 光标向上一行
          cursor_pos -= LINESIZE;
          if (cursor_pos < 0) cursor_pos += LINESIZE;
          return 0;

        case 'D': // Scroll forward
          scroll_up();
          return 0;

        case 'M': // Scroll reverse
          scroll_down();
          return 0;

        case 'G':  // Cursor home
          cursor_pos = 0;
          return 0;

        case '(': // Extended char set
          return 5;

        case 'c': // Reset
          cursor_pos = 0;
          video_color = ATTR_HEIJINGBAIZI;
          linewrap = 1;
          return 0;

        default:
          return 0;
      }

    case 2: // Seen Esc-[
      if (ch == '?') return 3;
      if (ch >= '0' && ch <= '9') {
        x = ch - '0';
        return 3;
      }

      if (ch == 's' || ch == 'u') {
        handle_sequence(0, 0, ch);
      } else if (ch == 'r' || ch == 'm') {
        handle_sequence(0, 0, ch);
      } else {
        handle_sequence(1, 1, ch);
      }
      return 0;

    case 3: // Seen Esc-[<digit>
      if (ch >= '0' && ch <= '9') {
        x = x * 10 + (ch - '0');
        return 3;
      }
      if (ch == ';') {
        y = 0;
        return 4;
      }
      handle_sequence(x, 1, ch);
      return 0;

    case 4:  // Seen Esc-[<digits>;
      if (ch >= '0' && ch <= '9') {
        y = y * 10 + (ch - '0');
        return 4;
      }
      handle_sequence(x, y, ch);
      return 0;

    case 5: // Seen Esc-(
      // 忽略字符集选择
      return 0;

    default:
      return 0;
  }
}

void print_buffer(const char *str, int len) {
  int ch;
  int i;
  unsigned char *p;
  char *end;
  unsigned char attr = video_color;

  if (!str) return;
  
  end = (char *) str + len;
  while (str < end) {
    ch = *str++;

    // 如果我们在一个多字符序列中，使用状态机处理它
    if (video_state > 0) {
      video_state = handle_multichar(video_state, ch);
      attr = video_color;
      continue;
    }

    if (ch >= ' ') {
      videoMem[cursor_pos++] = ch;
      videoMem[cursor_pos++] = attr;
    } else {
      switch (ch) {
        case 0:
          break;

        case '\n': // 新行
          cursor_pos = (cursor_pos / LINESIZE + 1) * LINESIZE;
          break;

        case '\r': // 回车
          cursor_pos = (cursor_pos / LINESIZE) * LINESIZE;
          break;

        case '\t':
          cursor_pos = (cursor_pos / (TABSTOP * CELLSIZE) + 1) * (TABSTOP * CELLSIZE);
          break;

        case 8: // 后退
          if (cursor_pos > 0) cursor_pos -= CELLSIZE;
          break;

        case 12: // 进纸，换行
          p = (unsigned char *) videoMem;
          for (i = 0; i < LIE * HANG; i++) {
            *p++ = ' ';
            *p++ = attr;
          }
          cursor_pos = 0;
          break;

        case 27: // 脱离
          video_state = 1;
          break;

        default: // 正常字符
          videoMem[cursor_pos++] = ch;
          videoMem[cursor_pos++] = attr;
      }
    }

    // 如果位置在屏幕外，则滚动
    if (cursor_pos >= SCREENSIZE) {
      if (linewrap) {
        scroll_up();
        cursor_pos -= LINESIZE;
      } else {
        cursor_pos = SCREENSIZE - CELLSIZE;
      }
    }
  }
}

void print_string(const char *str) {
  if (str) print_buffer(str, strlen(str));
  show_cursor();
}

void print_char(int ch) {
  char c = ch;
  print_buffer(&c, 1);
  show_cursor();
}

void clear_screen() {
  int i;
  unsigned char *p;
  unsigned char attr = video_color;

  // 用背景色填充屏幕
  p = (unsigned char *) videoMem;
  for (i = 0; i < LIE * HANG; i++)
  {
    *p++ = ' ';
    *p++ = attr;
  }

  // 将光标设置到屏幕的左上角
  cursor_pos = 0;
  show_cursor();
}

int screen_proc(struct proc_file *pf, void *arg) {
  int i, j;
  unsigned char *p;

  // 将屏幕转储到进程文件
  p = (unsigned char *) videoMem;
  for (i = 0; i < HANG; i++) {
    for (j = 0; j < LIE; j++) {
      proc_write(pf, p, 1);
      p += 2;
    }
    proc_write(pf, "\r\n", 2);
  }

  return 0;
}
